package com.optus.photoalbum.di

import com.google.gson.Gson
import com.optus.photoalbum.BuildConfig
import com.optus.photoalbum.data.api.utils.CallCoordinator
import com.optus.photoalbum.data.api.services.AlbumsApiService
import com.optus.photoalbum.data.api.services.UsersApiService
import com.optus.photoalbum.data.api.utils.TIMEOUT_IN_SEC
import dagger.Module
import dagger.Provides
import dagger.Reusable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@Module
class NetworkModule {
    /**
     * Provides an instance of GSON to any dependant consumer
     */
    @Provides
    @Reusable
    fun provideGson(): Gson = Gson()

    /**
     * Provides the http client required for Retrofit to make HTTP calls
     */
    @Provides
    @Reusable
    fun provideOkHttp(): OkHttpClient = OkHttpClient.Builder()
            .connectTimeout(TIMEOUT_IN_SEC, TimeUnit.SECONDS)
            .readTimeout(TIMEOUT_IN_SEC, TimeUnit.SECONDS)
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build()

    /**
     * Provides the Retrofit client required for making HTTP calls
     **/
    @Provides
    @Reusable
    fun provideRetrofitBuilder(okHttpClient: OkHttpClient): Retrofit =
            Retrofit.Builder().baseUrl(BuildConfig.BASE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build()

    @Provides
    @Reusable
    fun provideCallCoordinator(): CallCoordinator = CallCoordinator()

    /**
     * Provides the Retrofit client required for calling APIs from UsersApiService
     *
     * @see com.optus.photoalbum.data.api.services.UsersApiService
     */
    @Provides
    @Reusable
    fun provideUsersApiService(retrofit: Retrofit): UsersApiService =
            retrofit.create(UsersApiService::class.java)

    /**
     * Provides the Retrofit client required for calling APIs from AlbumsApiService
     *
     * @see com.optus.photoalbum.data.api.services.AlbumsApiService
     */
    @Provides
    @Reusable
    fun provideAlbumsApiService(retrofit: Retrofit): AlbumsApiService =
            retrofit.create(AlbumsApiService::class.java)
}