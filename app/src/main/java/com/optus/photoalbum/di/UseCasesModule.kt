package com.optus.photoalbum.di

import com.optus.photoalbum.base.Schedulers
import com.optus.photoalbum.data.domain.usecases.FetchAlbumsInfoUseCase
import com.optus.photoalbum.data.domain.usecases.FetchUsersInfoUseCase
import com.optus.photoalbum.data.managers.AlbumApiManager
import com.optus.photoalbum.data.managers.UsersApiManager
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module
class UseCasesModule {
    @Provides
    @Reusable
    fun provideSchedulers() = Schedulers()

    @Provides
    @Reusable
    fun provideFetchUsersInfoUseCase(usersApiManager: UsersApiManager, schedulers: Schedulers) =
            FetchUsersInfoUseCase(usersApiManager, schedulers)

    @Provides
    @Reusable
    fun provideFetchAlbumsInfoUseCase(albumApiManager: AlbumApiManager, schedulers: Schedulers) =
            FetchAlbumsInfoUseCase(albumApiManager, schedulers)
}