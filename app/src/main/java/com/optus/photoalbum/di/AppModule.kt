package com.optus.photoalbum.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.optus.photoalbum.base.AppViewModelFactory
import com.optus.photoalbum.presentation.errorhandling.ErrorComponentWrapper
import dagger.Module
import dagger.Provides
import dagger.Reusable
import javax.inject.Provider

@Module
class AppModule {
    /**
     * Singleton factory that searches generated map for specific provider and
     * uses it to get a ViewModel instance
     */
    @Provides
    fun provideViewModelFactory(providers: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>): ViewModelProvider.Factory =
            AppViewModelFactory(providers)

    @Provides
    @Reusable
    fun provideErrorComponentWrapper(): ErrorComponentWrapper = ErrorComponentWrapper()
}