package com.optus.photoalbum.di

import com.optus.photoalbum.presentation.albuminfo.AlbumInfoActivity
import com.optus.photoalbum.presentation.albuminfo.AlbumInfoActivityModule
import com.optus.photoalbum.presentation.userinfo.UserInfoActivity
import com.optus.photoalbum.presentation.userinfo.UserInfoActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Module that provides the dependencies to the activities in the project.
 */
@Module
abstract class ActivityBuilderModule {
    @ContributesAndroidInjector(modules = [UserInfoActivityModule::class, UserInfoActivityModule.InjectViewModel::class])
    abstract fun userInfoActivity(): UserInfoActivity

    @ContributesAndroidInjector(modules = [AlbumInfoActivityModule::class, AlbumInfoActivityModule.InjectViewModel::class])
    abstract fun albumInfoActivity(): AlbumInfoActivity
}