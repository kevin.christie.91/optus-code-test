package com.optus.photoalbum.di

import android.content.Context
import com.optus.photoalbum.PhotoAlbumApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule

@Component(
        modules = [
            AppModule::class,
            NetworkModule::class,
            ApiManagersModule::class,
            UseCasesModule::class,
            AndroidSupportInjectionModule::class,
            ActivityBuilderModule::class
        ]
)
interface PhotoAlbumAppComponent {
    fun inject(photoAlbumApplication: PhotoAlbumApplication)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(photoAlbumApplication: PhotoAlbumApplication): Builder

        @BindsInstance
        fun context(context: Context): Builder

        fun build(): PhotoAlbumAppComponent
    }
}