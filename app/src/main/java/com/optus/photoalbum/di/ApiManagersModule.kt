package com.optus.photoalbum.di

import com.optus.photoalbum.data.api.utils.CallCoordinator
import com.optus.photoalbum.data.api.services.AlbumsApiService
import com.optus.photoalbum.data.api.services.UsersApiService
import com.optus.photoalbum.data.managers.AlbumApiManager
import com.optus.photoalbum.data.managers.UsersApiManager
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module
class ApiManagersModule {
    @Provides
    @Reusable
    fun provideUserApiManager(callCoordinator: CallCoordinator, usersApiService: UsersApiService) =
            UsersApiManager(callCoordinator, usersApiService)

    @Provides
    @Reusable
    fun provideAlbumsApiManager(callCoordinator: CallCoordinator, albumsApiService: AlbumsApiService) =
            AlbumApiManager(callCoordinator, albumsApiService)
}