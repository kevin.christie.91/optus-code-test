package com.optus.photoalbum

import android.app.Activity
import android.app.Application
import com.optus.photoalbum.di.DaggerPhotoAlbumAppComponent
import com.optus.photoalbum.di.PhotoAlbumAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class PhotoAlbumApplication : Application(), HasActivityInjector {

    @Inject
    lateinit var activityDispatchingInjector: DispatchingAndroidInjector<Activity>

    lateinit var component: PhotoAlbumAppComponent

    override fun onCreate() {
        super.onCreate()
        component = DaggerPhotoAlbumAppComponent.builder()
                .application(this)
                .context(this)
                .build()
        component.inject(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> = this.activityDispatchingInjector

}