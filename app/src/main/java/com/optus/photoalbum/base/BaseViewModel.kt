package com.optus.photoalbum.base

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Base ViewModel class for all ViewModels that will contain boilerplate methods for common
 * functionalities
 */
abstract class BaseViewModel : ViewModel() {
    private val disposeOnClear = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        disposeOnClear.clear()
    }

    fun Disposable.untilCleared() =
            disposeOnClear.add(this)
}