package com.optus.photoalbum.base

import io.reactivex.android.schedulers.AndroidSchedulers

/**
 * Schedulers for rx
 **/
open class Schedulers {
    open fun ui() = AndroidSchedulers.mainThread()
    open fun io() = io.reactivex.schedulers.Schedulers.io()
}