package com.optus.photoalbum.base

import android.app.ProgressDialog
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.optus.photoalbum.R

/**
 * Base activity class for all activities that will contain boilerplate methods for common
 * functionalities
 */
abstract class BaseActivity : AppCompatActivity() {

    private lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        progressDialog = ProgressDialog(this, R.style.AppCompatAlertDialogStyle)
    }

    fun showLoading(msg: String = "Loading...") =
            progressDialog.let {
                it.setMessage(msg)
                it.setCancelable(false)
                it.show()
            }

    fun hideLoading() = progressDialog.let { it.hide() }
    abstract fun setUpViews()
    abstract fun hookUpObservers()
    abstract fun initErrorComponent()
}