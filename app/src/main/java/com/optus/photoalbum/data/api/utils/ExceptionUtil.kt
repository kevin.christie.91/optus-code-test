package com.optus.photoalbum.data.api.utils

import com.google.gson.stream.MalformedJsonException
import com.optus.photoalbum.data.domain.exceptions.ApplicationException
import com.optus.photoalbum.data.domain.exceptions.NetworkException
import com.optus.photoalbum.data.domain.exceptions.NoNetworkException
import com.optus.photoalbum.data.domain.exceptions.ServerException
import java.net.ConnectException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class ExceptionUtil {
    companion object {
        /**
         * This method is going to transform any exceptions
         * that occurred while executing the HTTP call
         */
        fun mapException(exception: Throwable): NetworkException {
            return when (exception) {
                is ConnectException -> NoNetworkException()
                is SocketTimeoutException -> ServerException()
                is UnknownHostException -> NoNetworkException()
                is SocketException -> NoNetworkException()
                is MalformedJsonException -> ServerException()
                is ApplicationException -> exception
                else -> ApplicationException()
            }
        }
    }
}