package com.optus.photoalbum.data.api.models.response.fetchuserinfo

data class Company(
        val bs: String,
        val catchPhrase: String,
        val name: String
)