package com.optus.photoalbum.data.managers

import com.optus.photoalbum.data.api.utils.CallCoordinator
import com.optus.photoalbum.data.api.models.response.fetchalbuminfo.FetchAlbumInfoResponse
import com.optus.photoalbum.data.api.services.AlbumsApiService
import com.optus.photoalbum.data.domain.repositories.AlbumRepository
import io.reactivex.Single

class AlbumApiManager(private val callCoordinator: CallCoordinator,
                      private val albumsApiService: AlbumsApiService) : AlbumRepository {
    override fun fetchAlbumsInfo(): Single<FetchAlbumInfoResponse> {
        return Single.fromCallable {
            val response = callCoordinator.execute(albumsApiService.fetchAlbumsInfo())
            response
        }
    }

}