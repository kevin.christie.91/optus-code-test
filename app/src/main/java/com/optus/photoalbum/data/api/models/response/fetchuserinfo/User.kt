package com.optus.photoalbum.data.api.models.response.fetchuserinfo

data class User(
        val address: Address,
        val company: Company,
        val email: String,
        val id: Int,
        val name: String,
        val phone: String,
        val username: String,
        val website: String
)