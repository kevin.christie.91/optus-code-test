package com.optus.photoalbum.data.domain.exceptions

/**
 *  Network exception
 *
 *  No internet connection or similar
 */
class NoNetworkException : NetworkException()