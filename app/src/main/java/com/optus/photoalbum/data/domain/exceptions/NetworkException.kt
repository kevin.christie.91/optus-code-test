package com.optus.photoalbum.data.domain.exceptions

/**
 * Network error
 *
 * Got error code from server or something failed while making API call/parsing
 * Can be ok or retry
 */
open class NetworkException : BaseException() {
    var canRetry: Boolean = true
}