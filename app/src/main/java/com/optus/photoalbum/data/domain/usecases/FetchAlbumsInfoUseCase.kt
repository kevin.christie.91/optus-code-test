package com.optus.photoalbum.data.domain.usecases

import com.optus.photoalbum.base.Schedulers
import com.optus.photoalbum.data.api.models.response.fetchalbuminfo.FetchAlbumInfoResponse
import com.optus.photoalbum.data.domain.repositories.AlbumRepository
import io.reactivex.Single

class FetchAlbumsInfoUseCase(private val albumRepository: AlbumRepository, private val schedulers: Schedulers) {
    fun execute(): Single<FetchAlbumInfoResponse> {
        return albumRepository.fetchAlbumsInfo()
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
    }
}