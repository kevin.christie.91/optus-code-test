package com.optus.photoalbum.data.api.models.response.fetchuserinfo

data class Geo(
        val lat: String,
        val lng: String
)