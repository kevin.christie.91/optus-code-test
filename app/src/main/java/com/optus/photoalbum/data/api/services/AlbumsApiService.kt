package com.optus.photoalbum.data.api.services

import com.optus.photoalbum.data.api.models.response.fetchalbuminfo.FetchAlbumInfoResponse
import com.optus.photoalbum.data.api.utils.PHOTOS_INFO_ENDPOINT
import retrofit2.Call
import retrofit2.http.GET

/**
 * All Albums related APIs
 **/
interface AlbumsApiService {
    @GET(PHOTOS_INFO_ENDPOINT)
    fun fetchAlbumsInfo(): Call<FetchAlbumInfoResponse>
}