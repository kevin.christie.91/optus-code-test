package com.optus.photoalbum.data.domain.exceptions

/**
 * Network exception
 *
 * Server connection error or malformed json
 */
class ServerException : NetworkException()