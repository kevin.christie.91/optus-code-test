package com.optus.photoalbum.data.api.models.response.fetchuserinfo

data class Address(
        val city: String,
        val geo: Geo,
        val street: String,
        val suite: String,
        val zipcode: String
)