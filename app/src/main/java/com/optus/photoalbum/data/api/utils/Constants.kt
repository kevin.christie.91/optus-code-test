package com.optus.photoalbum.data.api.utils

/** Timeout of all REST API HTTP calls */
const val TIMEOUT_IN_SEC: Long = 60

const val USER_INFO_ENDPOINT = "users"
const val PHOTOS_INFO_ENDPOINT = "photos"