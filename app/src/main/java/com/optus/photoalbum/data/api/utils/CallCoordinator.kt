package com.optus.photoalbum.data.api.utils

import android.util.Log
import android.util.MalformedJsonException
import com.optus.photoalbum.data.domain.exceptions.BaseException
import retrofit2.Call
import java.net.ConnectException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.inject.Singleton
import javax.net.ssl.SSLHandshakeException

@Singleton
class CallCoordinator {
    /**
     * Synchronously executes the provided call and, if successful, returns the parsed response
     * body. If the call is not successful an attempt is made to parse the response error body.
     */
    @Throws(
            BaseException::class, ConnectException::class, SocketTimeoutException::class,
            UnknownHostException::class, SSLHandshakeException::class, SocketException::class,
            MalformedJsonException::class)
    fun <T> execute(call: Call<T>): T? {
        try {
            val response = call.execute()
            return response.body()
        } catch (exception: Exception) {
            Log.e("CallCoordinator execute", exception.message ?: "")
            // map exceptions
            throw ExceptionUtil.mapException(exception)
        }
    }

}