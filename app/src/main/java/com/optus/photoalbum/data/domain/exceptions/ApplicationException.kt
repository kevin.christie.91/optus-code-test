package com.optus.photoalbum.data.domain.exceptions

/**
 * Network exception
 *
 * Server answered and returned some error
 */
open class ApplicationException : NetworkException()