package com.optus.photoalbum.data.managers

import com.optus.photoalbum.data.api.utils.CallCoordinator
import com.optus.photoalbum.data.api.models.response.fetchuserinfo.FetchUsersInfoResponse
import com.optus.photoalbum.data.api.services.UsersApiService
import com.optus.photoalbum.data.domain.repositories.UserRepository
import io.reactivex.Single

class UsersApiManager(private val callCoordinator: CallCoordinator,
                      private val usersApiService: UsersApiService) : UserRepository {
    override fun fetchUserInfo(): Single<FetchUsersInfoResponse> {
        return Single.fromCallable {
            val response = callCoordinator.execute(usersApiService.fetchUsersInfo())
            response
        }
    }
}