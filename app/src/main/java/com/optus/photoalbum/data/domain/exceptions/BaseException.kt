package com.optus.photoalbum.data.domain.exceptions

open class BaseException : Exception()