package com.optus.photoalbum.data.api.services

import com.optus.photoalbum.data.api.models.response.fetchuserinfo.FetchUsersInfoResponse
import com.optus.photoalbum.data.api.utils.USER_INFO_ENDPOINT
import retrofit2.Call
import retrofit2.http.GET

/**
 * All User related APIs
 **/
interface UsersApiService {
    @GET(USER_INFO_ENDPOINT)
    fun fetchUsersInfo(): Call<FetchUsersInfoResponse>
}