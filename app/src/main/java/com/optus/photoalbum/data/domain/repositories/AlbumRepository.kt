package com.optus.photoalbum.data.domain.repositories

import com.optus.photoalbum.data.api.models.response.fetchalbuminfo.FetchAlbumInfoResponse
import io.reactivex.Single

/**
 * Contract for calling Album services
 * */
interface AlbumRepository {
    fun fetchAlbumsInfo(): Single<FetchAlbumInfoResponse>
}