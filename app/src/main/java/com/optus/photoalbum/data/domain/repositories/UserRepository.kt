package com.optus.photoalbum.data.domain.repositories

import com.optus.photoalbum.data.api.models.response.fetchuserinfo.FetchUsersInfoResponse
import io.reactivex.Single

/**
 * Contract for calling User services
 * */
interface UserRepository {
    fun fetchUserInfo(): Single<FetchUsersInfoResponse>
}
