package com.optus.photoalbum.data.domain.usecases

import com.optus.photoalbum.base.Schedulers
import com.optus.photoalbum.data.api.models.response.fetchuserinfo.FetchUsersInfoResponse
import com.optus.photoalbum.data.domain.repositories.UserRepository
import io.reactivex.Single

class FetchUsersInfoUseCase(private val userRepository: UserRepository, private val schedulers: Schedulers) {
    fun execute(): Single<FetchUsersInfoResponse> {
        return userRepository.fetchUserInfo()
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
    }
}