package com.optus.photoalbum.presentation.extras

import android.content.Context
import android.view.View
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.core.view.ViewCompat
import com.google.android.material.snackbar.Snackbar

//Snackbar
fun createSnackbar(positiveButtonTextResId: Int?, messageResId: Int, action: (() -> Unit)?, parentView: View): Snackbar {
    val ELEVATION = 6f
    val DURATION_MS = 5000

    val snackbar = Snackbar.make(parentView, messageResId, DURATION_MS)

    with(snackbar.view) {
        positiveButtonTextResId?.let {
            snackbar.setAction(it) {
                action?.invoke()
            }
            val buttons = ArrayList<View>()
            findViewsWithText(buttons, context.getString(positiveButtonTextResId),
                    View.FIND_VIEWS_WITH_TEXT
            )
            if (buttons.isNotEmpty()) {
                (buttons.first() as TextView).isAllCaps = false
            }
        }

        ViewCompat.setElevation(this, ELEVATION)
    }

    return snackbar
}

// Dialog
fun showSingleActionDialog(context: Context,
                           @StringRes titleResId: Int,
                           @StringRes messageResId: Int,
                           @StringRes positiveButtonTextResId: Int,
                           positiveAction: () -> Unit,
                           cancellable: Boolean = false): AlertDialog {
    return showActionDialog(context, context.getString(titleResId), context.getString(messageResId),
            positiveButtonTextResId, positiveAction, cancellable)
}

private fun showActionDialog(context: Context,
                             title: String,
                             message: String?,
                             @StringRes positiveButtonTextResId: Int?,
                             positiveAction: () -> Unit?,
                             cancellable: Boolean = false): AlertDialog {
    val alertDialogBuilder = AlertDialog.Builder(context)
            .setCancelable(cancellable)
            .setTitle(title)

    positiveButtonTextResId?.let {
        alertDialogBuilder.setPositiveButton(context.getString(positiveButtonTextResId)) { _, _ -> positiveAction() }
    }

    message?.let {
        alertDialogBuilder.setMessage(it)
    }

    val alertDialog = alertDialogBuilder.create()
    alertDialog.show()

    return alertDialog
}