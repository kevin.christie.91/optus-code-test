package com.optus.photoalbum.presentation.albuminfo

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.optus.photoalbum.R
import com.optus.photoalbum.data.api.models.response.fetchalbuminfo.Photo
import com.optus.photoalbum.databinding.AlbumItemLayoutBinding

class AlbumsListAdapter(
        private val album: List<Photo>,
        private val albumClickListener: AlbumClickListener
) : RecyclerView.Adapter<AlbumsListAdapter.AlbumViewHolder>() {

    class AlbumViewHolder(binding: AlbumItemLayoutBinding) : RecyclerView.ViewHolder(binding.root) {
        val binding = binding
        fun bind(photoItem: Photo) {
            binding.apply {
                photo = photoItem
                executePendingBindings()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: AlbumItemLayoutBinding =
                DataBindingUtil.inflate(inflater, R.layout.album_item_layout, parent, false)
        binding.clickListener = albumClickListener
        return AlbumViewHolder(binding)
    }

    override fun getItemCount() = album.size

    override fun onBindViewHolder(holder: AlbumViewHolder, position: Int) {
        holder.bind(album[position])
        with(holder.binding.ivThumbnail) {
            Glide.with(this.context)
                    .load(album[position].thumbnailUrl)
                    .centerCrop()
                    .placeholder(R.drawable.ic_img_placeholder)
                    .error(R.drawable.ic_error)
                    .into(this)
        }

    }

}