package com.optus.photoalbum.presentation.userinfo

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.optus.photoalbum.R
import com.optus.photoalbum.data.api.models.response.fetchuserinfo.User
import com.optus.photoalbum.databinding.UserInfoItemLayoutBinding

class UsersAdapter(private val usersInfoList: List<User>,
                   private val userItemClickListener: UserItemClickListener) : RecyclerView.Adapter<UsersAdapter.UsersViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: UserInfoItemLayoutBinding = DataBindingUtil.inflate(inflater, R.layout.user_info_item_layout, parent, false)
        binding.clickListener = userItemClickListener
        return UsersViewHolder(binding)
    }

    override fun getItemCount() = usersInfoList.size

    override fun onBindViewHolder(holder: UsersViewHolder, position: Int) {
        holder.bind(usersInfoList[position])
    }

    class UsersViewHolder(private val binding: UserInfoItemLayoutBinding) :
            RecyclerView.ViewHolder(binding.root) {
        fun bind(userItem: User) {
            binding.apply {
                user = userItem
                executePendingBindings()
            }
        }
    }
}