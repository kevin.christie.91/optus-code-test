package com.optus.photoalbum.presentation.userinfo

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.optus.photoalbum.Error
import com.optus.photoalbum.Loading
import com.optus.photoalbum.R
import com.optus.photoalbum.Success
import com.optus.photoalbum.base.BaseActivity
import com.optus.photoalbum.data.api.models.response.fetchuserinfo.FetchUsersInfoResponse
import com.optus.photoalbum.data.domain.exceptions.NoNetworkException
import com.optus.photoalbum.databinding.ActivityUserInfoBinding
import com.optus.photoalbum.presentation.albuminfo.AlbumInfoActivity
import com.optus.photoalbum.presentation.errorhandling.DialogErrorComponent
import com.optus.photoalbum.presentation.errorhandling.ErrorComponentWrapper
import com.optus.photoalbum.presentation.errorhandling.SnackBarErrorComponent
import dagger.android.AndroidInjection
import javax.inject.Inject

class UserInfoActivity : BaseActivity(), UserItemClickListener {

    @Inject
    lateinit var viewModel: UserInfoViewModel

    @Inject
    lateinit var snackBarErrorComponent: ErrorComponentWrapper

    @Inject
    lateinit var dialogErrorComponent: ErrorComponentWrapper

    private lateinit var binding: ActivityUserInfoBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_info)
        setUpViews()
        initErrorComponent()
        hookUpObservers()
        viewModel.fetchUsers()
    }

    override fun setUpViews() {
        supportActionBar?.title = getString(R.string.user_info)
        binding.recyclerUsersList.layoutManager = LinearLayoutManager(this)
    }

    override fun hookUpObservers() {
        viewModel.getViewState().observe(this, Observer { viewState ->
            when (viewState) {
                is Loading -> {
                    showLoading()
                }
                is Success<*> -> {
                    hideLoading()
                    val usersInfo = viewState.data as FetchUsersInfoResponse
                    binding.recyclerUsersList.adapter = UsersAdapter(usersInfo, this)
                }
                is Error -> {
                    hideLoading()
                    if (viewState.exception is NoNetworkException) {
                        snackBarErrorComponent.handleError(viewState.exception, viewModel::fetchUsers)
                    } else {
                        dialogErrorComponent.handleError(viewState.exception, viewModel::fetchUsers)
                    }
                }
            }
        })
    }

    override fun initErrorComponent() {
        snackBarErrorComponent.visualComponent = SnackBarErrorComponent(binding.recyclerUsersList)
        dialogErrorComponent.visualComponent = DialogErrorComponent(this)
    }

    override fun onUserClick(albumId: Int) {
        startActivity(AlbumInfoActivity.newIntent(this, albumId))
    }
}
