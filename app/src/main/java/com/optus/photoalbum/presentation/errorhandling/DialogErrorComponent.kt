package com.optus.photoalbum.presentation.errorhandling

import android.content.Context
import com.optus.photoalbum.presentation.errorhandling.ErrorComponentInterface.ErrorMessages.genericErrorMsg
import com.optus.photoalbum.presentation.errorhandling.ErrorComponentInterface.ErrorMessages.genericErrorTitle
import com.optus.photoalbum.presentation.errorhandling.ErrorComponentInterface.ErrorMessages.retry
import com.optus.photoalbum.presentation.extras.showSingleActionDialog

/**
 * Error component that displays errors in a dialog
 */
class DialogErrorComponent(private val context: Context) : ErrorComponentInterface {

    override fun showNetworkNotFoundError(exception: Throwable, methodToInvoke: (() -> Any)?) {
        //Handled by SnackBar error component
    }

    override fun showServerError(exception: Throwable, methodToInvoke: (() -> Any)?) {
        showSingleActionDialog(context,
                genericErrorTitle,
                genericErrorMsg,
                retry,
                { methodToInvoke?.invoke() }
        )
    }
}