package com.optus.photoalbum.presentation.errorhandling

import android.view.View
import androidx.annotation.StringRes
import com.google.android.material.snackbar.Snackbar
import com.optus.photoalbum.presentation.errorhandling.ErrorComponentInterface.ErrorMessages.networkErrorTitle
import com.optus.photoalbum.presentation.extras.createSnackbar

/**
 * A UI error component in the form of a snackbar for showing errors which do not need to block the UI
 * for user to take action (like dialog boxes)
 *
 * This error component will display a snackbar which will show the error message with or withut an action
 * button.
 **/
class SnackBarErrorComponent(private val parentView: View, val shouldShowActionButton: Boolean = true) :
        ErrorComponentInterface {
    private var snackbar: Snackbar? = null

    override fun showNetworkNotFoundError(exception: Throwable, methodToInvoke: (() -> Any)?) {
        show(networkErrorTitle, ErrorComponentInterface.retry, methodToInvoke)
    }

    override fun showServerError(exception: Throwable, methodToInvoke: (() -> Any)?) {
        //Handled by Dialog Error component
    }

    private fun show(@StringRes messageResId: Int, @StringRes positiveButtonTextResId: Int,
                     positiveAction: (() -> Any)?, trackActionMethod: (() -> Unit)? = null) {
        if (shouldShowActionButton) {
            snackbar = createSnackbar(
                    positiveButtonTextResId, messageResId, {
                positiveAction?.invoke()
                trackActionMethod?.invoke()
            }, parentView
            )
            snackbar?.show()
        } else {
            show(messageResId, trackActionMethod)
        }
    }

    private fun show(@StringRes messageResId: Int, trackActionMethod: (() -> Unit)? = null) {
        snackbar = createSnackbar(
                null, messageResId,
                { trackActionMethod?.invoke() }, parentView
        )
        snackbar?.show()
    }
}