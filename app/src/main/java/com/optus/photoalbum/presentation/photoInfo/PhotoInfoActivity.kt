package com.optus.photoalbum.presentation.photoInfo

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.optus.photoalbum.R
import com.optus.photoalbum.base.BaseActivity
import com.optus.photoalbum.databinding.ActivityPhotoInfoBinding


class PhotoInfoActivity : BaseActivity() {
    companion object {
        const val URL = "url"
        const val TITLE = "title"
        const val PHOTO_ID = "photo_id"
        const val ALBUM_ID = "album_id"

        fun newIntent(context: Context, url: String, title: String, albumId: String, photoId: String): Intent {
            return Intent(context, PhotoInfoActivity::class.java).apply {
                putExtra(URL, url)
                putExtra(TITLE, title)
                putExtra(PHOTO_ID, photoId)
                putExtra(ALBUM_ID, albumId)
            }
        }
    }

    private lateinit var binding: ActivityPhotoInfoBinding

    private val photoUrl: String by lazy {
        intent.getStringExtra(URL)
    }

    private val photoTitle: String by lazy {
        intent.getStringExtra(TITLE)
    }

    private val photoId: String by lazy {
        intent.getStringExtra(PHOTO_ID)
    }

    private val albumId: String by lazy {
        intent.getStringExtra(ALBUM_ID)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_photo_info)
        setUpViews()
    }

    override fun setUpViews() {
        supportActionBar?.title = String.format(getString(R.string.photoInfo), albumId, photoId)
        binding.title = photoTitle
        Glide.with(this)
                .load(photoUrl)
                .centerCrop()
                .placeholder(R.drawable.ic_img_placeholder)
                .error(R.drawable.ic_error)
                .into(binding.ivPhoto)
    }

    override fun hookUpObservers() {
        //Empty
    }

    override fun initErrorComponent() {
        //Empty
    }
}
