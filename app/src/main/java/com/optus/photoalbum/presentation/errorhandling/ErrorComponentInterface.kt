package com.optus.photoalbum.presentation.errorhandling

import androidx.annotation.StringRes
import com.optus.photoalbum.R
import com.optus.photoalbum.data.domain.exceptions.ApplicationException
import com.optus.photoalbum.data.domain.exceptions.NoNetworkException
import com.optus.photoalbum.data.domain.exceptions.ServerException
import io.reactivex.exceptions.CompositeException

/**
 * Error component contract which will allow various error components eg: snackBar, dialogs,
 * inline-embedded errors to handle the exceptions.
 **/
interface ErrorComponentInterface {

    companion object ErrorMessages {
        @StringRes
        const val networkErrorTitle = R.string.check_connection

        @StringRes
        const val retry = R.string.retry

        @StringRes
        const val ok = R.string.ok

        @StringRes
        const val genericErrorTitle = R.string.generic_error_title

        @StringRes
        const val genericErrorMsg = R.string.something_went_wrong
    }

    fun handleError(throwable: Throwable, methodToInvoke: (() -> Any)) {
        throwable.printStackTrace()

        val exception = if (throwable is CompositeException) throwable.exceptions.first() else throwable

        exception.printStackTrace()

        when (exception) {
            is NoNetworkException -> showNetworkNotFoundError(exception, methodToInvoke)
            is ServerException -> showServerError(exception, methodToInvoke)
            is ApplicationException -> showServerError(exception, methodToInvoke)
            //Can add various other exceptions to be handled
        }
    }

    fun showNetworkNotFoundError(exception: Throwable, methodToInvoke: (() -> Any)? = null)

    fun showServerError(exception: Throwable, methodToInvoke: (() -> Any)? = null)
}