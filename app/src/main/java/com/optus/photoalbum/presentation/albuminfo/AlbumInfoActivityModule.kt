package com.optus.photoalbum.presentation.albuminfo

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.optus.photoalbum.data.domain.usecases.FetchAlbumsInfoUseCase
import com.optus.photoalbum.di.annotations.ViewModelKey
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

/**
 * Module that provides the required dependencies to the MainActivity class.
 *
 * @see com.optus.photoalbum.presentation.albuminfo.AlbumInfoActivity
 */
@Module(
        includes = [
            AlbumInfoActivityModule.ProvideViewModel::class]
)
class AlbumInfoActivityModule {
    /**
     * Associate this provider method with ViewModel type in a generated map
     */
    @Module
    class ProvideViewModel {
        @Provides
        @IntoMap
        @ViewModelKey(AlbumInfoViewModel::class)
        fun provideUserInfoActivityViewModel(fetchAlbumsInfoUseCase: FetchAlbumsInfoUseCase): ViewModel =
                AlbumInfoViewModel(fetchAlbumsInfoUseCase)
    }

    @Module
    class InjectViewModel {
        @Provides
        fun provideAlbumInfoActivityViewModel(
                factory: ViewModelProvider.Factory,
                target: AlbumInfoActivity
        ): AlbumInfoViewModel =
                ViewModelProviders.of(target, factory).get(AlbumInfoViewModel::class.java)
    }
}