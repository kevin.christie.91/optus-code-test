package com.optus.photoalbum.presentation.userinfo

interface UserItemClickListener {
    fun onUserClick(albumId: Int)
}