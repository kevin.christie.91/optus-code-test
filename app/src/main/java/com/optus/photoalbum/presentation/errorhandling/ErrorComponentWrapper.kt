package com.optus.photoalbum.presentation.errorhandling

/**
 * Error Handling wrapper for handling any exceptions in the app
 * passes exception to [ErrorComponentInterface] to show it to user

 * Note: for now, only connectivity errors are being handled.
 **/
class ErrorComponentWrapper {
    var visualComponent: ErrorComponentInterface? = null

    fun handleError(throwable: Throwable, methodToInvoke: (() -> Any)) {
        showError(throwable, methodToInvoke)
    }

    private fun showError(throwable: Throwable, methodToInvoke: (() -> Any)) {
        visualComponent!!.handleError(throwable, methodToInvoke)
    }
}