package com.optus.photoalbum.presentation.userinfo

import androidx.lifecycle.MutableLiveData
import com.optus.photoalbum.Loading
import com.optus.photoalbum.Success
import com.optus.photoalbum.Error
import com.optus.photoalbum.ViewState
import com.optus.photoalbum.base.BaseViewModel
import com.optus.photoalbum.data.domain.usecases.FetchUsersInfoUseCase
import io.reactivex.disposables.CompositeDisposable

class UserInfoViewModel(val useCase: FetchUsersInfoUseCase) : BaseViewModel() {

    private val disposables = CompositeDisposable()
    private val usersListViewState: MutableLiveData<ViewState> = MutableLiveData()
    fun getViewState() = usersListViewState

    fun fetchUsers() {
        disposables.add(useCase.execute()
                .doOnSubscribe {
                    usersListViewState.value = Loading
                }
                .subscribe({
                    usersListViewState.value = Success(it)
                }, {
                    usersListViewState.value = Error(it)
                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }
}