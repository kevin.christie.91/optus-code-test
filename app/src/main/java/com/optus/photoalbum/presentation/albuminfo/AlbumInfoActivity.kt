package com.optus.photoalbum.presentation.albuminfo

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.optus.photoalbum.Error
import com.optus.photoalbum.Loading
import com.optus.photoalbum.R
import com.optus.photoalbum.Success
import com.optus.photoalbum.base.BaseActivity
import com.optus.photoalbum.data.api.models.response.fetchalbuminfo.Photo
import com.optus.photoalbum.data.domain.exceptions.NoNetworkException
import com.optus.photoalbum.databinding.ActivityAlbumInfoBinding
import com.optus.photoalbum.presentation.errorhandling.DialogErrorComponent
import com.optus.photoalbum.presentation.errorhandling.ErrorComponentWrapper
import com.optus.photoalbum.presentation.errorhandling.SnackBarErrorComponent
import com.optus.photoalbum.presentation.photoInfo.PhotoInfoActivity
import dagger.android.AndroidInjection
import javax.inject.Inject

class AlbumInfoActivity : BaseActivity(), AlbumClickListener {
    companion object {
        const val ALBUM_ID = "album_id"

        fun newIntent(context: Context, albumId: Int): Intent {
            return Intent(context, AlbumInfoActivity::class.java).putExtra(ALBUM_ID, albumId)
        }
    }

    @Inject
    lateinit var viewModel: AlbumInfoViewModel

    @Inject
    lateinit var snackBarErrorComponent: ErrorComponentWrapper

    @Inject
    lateinit var dialogErrorComponent: ErrorComponentWrapper

    private lateinit var binding: ActivityAlbumInfoBinding

    private val albumId: Int by lazy {
        intent.getIntExtra(ALBUM_ID, 0)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_album_info)
        setUpViews()
        initErrorComponent()
        hookUpObservers()
        viewModel.fetchAlbumsInfo(albumId)
    }

    override fun setUpViews() {
        supportActionBar?.title = String.format(getString(R.string.album_info), albumId)
        binding.recyclerAlbumsList.layoutManager = LinearLayoutManager(this)
    }

    override fun hookUpObservers() {
        viewModel.getViewState().observe(this, Observer { viewState ->
            when (viewState) {
                is Loading -> {
                    showLoading()
                }
                is Success<*> -> {
                    hideLoading()
                    val albumsList = viewState.data as List<Photo>
                    binding.recyclerAlbumsList.adapter = AlbumsListAdapter(albumsList, this)
                }
                is Error -> {
                    hideLoading()
                    if (viewState.exception is NoNetworkException) {
                        snackBarErrorComponent.handleError(viewState.exception) { viewModel.fetchAlbumsInfo(albumId) }
                    } else {
                        dialogErrorComponent.handleError(viewState.exception) { viewModel.fetchAlbumsInfo(albumId) }
                    }
                }
            }
        })
    }

    override fun initErrorComponent() {
        snackBarErrorComponent.visualComponent = SnackBarErrorComponent(binding.recyclerAlbumsList)
        dialogErrorComponent.visualComponent = DialogErrorComponent(this)
    }

    override fun onPhotoClick(photo: Photo) {
        startActivity(PhotoInfoActivity.newIntent(this, photo.url, photo.title, photo.albumId.toString(), photo.id.toString()))
    }
}
