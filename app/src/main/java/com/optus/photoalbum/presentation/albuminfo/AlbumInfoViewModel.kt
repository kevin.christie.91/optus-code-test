package com.optus.photoalbum.presentation.albuminfo

import androidx.lifecycle.MutableLiveData
import com.optus.photoalbum.Error
import com.optus.photoalbum.Loading
import com.optus.photoalbum.Success
import com.optus.photoalbum.ViewState
import com.optus.photoalbum.base.BaseViewModel
import com.optus.photoalbum.data.domain.usecases.FetchAlbumsInfoUseCase
import io.reactivex.disposables.CompositeDisposable

class AlbumInfoViewModel(private val albumsInfoUseCase: FetchAlbumsInfoUseCase) : BaseViewModel() {
    private val disposables = CompositeDisposable()
    private val albumsListViewState: MutableLiveData<ViewState> = MutableLiveData()
    fun getViewState() = albumsListViewState

    fun fetchAlbumsInfo(id: Int) {
        disposables.add(albumsInfoUseCase.execute()
                .doOnSubscribe {
                    albumsListViewState.value = Loading
                }
                .subscribe({ albumsList ->
                    albumsListViewState.value = Success(albumsList.filter { it.albumId == id })
                }, {
                    albumsListViewState.value = Error(it)
                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }
}