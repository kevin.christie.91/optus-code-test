package com.optus.photoalbum.presentation.userinfo

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.optus.photoalbum.di.annotations.ViewModelKey
import com.optus.photoalbum.data.domain.usecases.FetchUsersInfoUseCase
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

/**
 * Module that provides the required dependencies to the MainActivity class.
 *
 * @see com.optus.photoalbum.presentation.userinfo.UserInfoActivity
 */
@Module(
        includes = [
            UserInfoActivityModule.ProvideViewModel::class]
)
abstract class UserInfoActivityModule {
    /**
     * Associate this provider method with ViewModel type in a generated map
     */
    @Module
    class ProvideViewModel {
        @Provides
        @IntoMap
        @ViewModelKey(UserInfoViewModel::class)
        fun provideUserInfoActivityViewModel(fetchUsersInfoUseCase: FetchUsersInfoUseCase): ViewModel =
                UserInfoViewModel(fetchUsersInfoUseCase)
    }

    @Module
    class InjectViewModel {
        @Provides
        fun provideUserInfoActivityViewModel(
                factory: ViewModelProvider.Factory,
                target: UserInfoActivity
        ): UserInfoViewModel =
                ViewModelProviders.of(target, factory).get(UserInfoViewModel::class.java)
    }
}