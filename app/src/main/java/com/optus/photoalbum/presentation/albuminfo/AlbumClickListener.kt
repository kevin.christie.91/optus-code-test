package com.optus.photoalbum.presentation.albuminfo

import com.optus.photoalbum.data.api.models.response.fetchalbuminfo.Photo

interface AlbumClickListener {
    fun onPhotoClick(photo: Photo)
}