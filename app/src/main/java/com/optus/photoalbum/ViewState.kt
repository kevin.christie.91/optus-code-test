package com.optus.photoalbum

sealed class ViewState

object Loading : ViewState()

class Error(val exception: Throwable) : ViewState()

class Success<T>(val data: T) : ViewState()