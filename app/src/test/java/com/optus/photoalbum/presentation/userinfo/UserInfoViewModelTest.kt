package com.optus.photoalbum.presentation.userinfo

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockito_kotlin.mock
import com.optus.photoalbum.Error
import com.optus.photoalbum.Loading
import com.optus.photoalbum.Success
import com.optus.photoalbum.base.testObserver
import com.optus.photoalbum.data.api.models.response.fetchuserinfo.FetchUsersInfoResponse
import com.optus.photoalbum.data.domain.usecases.FetchUsersInfoUseCase
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.junit.BeforeClass
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import kotlin.test.assertEquals

class UserInfoViewModelTest {
    companion object {
        @BeforeClass
        @JvmStatic fun setUpRxAndroidPlugins(){
            var immediate = object: Scheduler(){
                override fun createWorker(): Worker {
                    return ExecutorScheduler.ExecutorWorker(Runnable::run)
                }
            }
            RxJavaPlugins.setInitIoSchedulerHandler { scheduler -> immediate }
            RxJavaPlugins.setInitComputationSchedulerHandler { scheduler -> immediate }
            RxJavaPlugins.setInitNewThreadSchedulerHandler { scheduler -> immediate }
            RxJavaPlugins.setInitSingleSchedulerHandler { scheduler -> immediate }
            RxAndroidPlugins.setInitMainThreadSchedulerHandler { scheduler -> immediate }
        }
    }

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private val useCase: FetchUsersInfoUseCase = mock()
    private val viewModel = UserInfoViewModel(useCase)

    @Test
    fun `on fetchUsers - is successful - loading and success view states are received`(){
        val response: FetchUsersInfoResponse = mock()
        val testObserver = viewModel.getViewState().testObserver()

        Mockito.`when`(useCase.execute()).thenReturn(Single.just(response))

        viewModel.fetchUsers()

        assertEquals(testObserver.observedValues.size, 2, "Two view states should be received")
        assert(testObserver.observedValues[0] is Loading)
        assert(testObserver.observedValues[1] is Success<*>)
        assert((testObserver.observedValues[1] as Success<*>).data is List<*>)
    }

    @Test
    fun `on fetchUsers - is not successful - loading and error view states are received`(){
        val exception = Throwable()
        val testObserver = viewModel.getViewState().testObserver()

        Mockito.`when`(useCase.execute()).thenReturn(Single.error(exception))

        viewModel.fetchUsers()

        assertEquals(testObserver.observedValues.size, 2, "Two view states should be received")
        assert(testObserver.observedValues[0] is Loading)
        assert(testObserver.observedValues[1] is Error)
        assert((testObserver.observedValues[1] as Error).exception == exception)
    }
}