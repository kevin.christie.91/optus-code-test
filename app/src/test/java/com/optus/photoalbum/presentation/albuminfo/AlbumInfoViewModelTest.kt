package com.optus.photoalbum.presentation.albuminfo

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockito_kotlin.mock
import com.optus.photoalbum.Error
import com.optus.photoalbum.Loading
import com.optus.photoalbum.Success
import com.optus.photoalbum.base.testObserver
import com.optus.photoalbum.data.api.models.response.fetchalbuminfo.FetchAlbumInfoResponse
import com.optus.photoalbum.data.api.models.response.fetchalbuminfo.Photo
import com.optus.photoalbum.data.domain.usecases.FetchAlbumsInfoUseCase
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.junit.Test

import org.junit.Assert.*
import org.junit.BeforeClass
import org.junit.Rule
import org.mockito.Mockito
import kotlin.test.assertEquals

class AlbumInfoViewModelTest {
    companion object {
        @BeforeClass
        @JvmStatic
        fun setUpRxAndroidPlugins() {
            var immediate = object : Scheduler() {
                override fun createWorker(): Worker {
                    return ExecutorScheduler.ExecutorWorker(Runnable::run)
                }
            }
            RxJavaPlugins.setInitIoSchedulerHandler { scheduler -> immediate }
            RxJavaPlugins.setInitComputationSchedulerHandler { scheduler -> immediate }
            RxJavaPlugins.setInitNewThreadSchedulerHandler { scheduler -> immediate }
            RxJavaPlugins.setInitSingleSchedulerHandler { scheduler -> immediate }
            RxAndroidPlugins.setInitMainThreadSchedulerHandler { scheduler -> immediate }
        }
    }

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private val useCase: FetchAlbumsInfoUseCase = mock()
    private val viewModel = AlbumInfoViewModel(useCase)

    @Test
    fun `on fetchUsers - is not successful - loading and error view states are received`(){
        val exception = Throwable()
        val testObserver = viewModel.getViewState().testObserver()

        Mockito.`when`(useCase.execute()).thenReturn(Single.error(exception))

        viewModel.fetchAlbumsInfo(1)

        assertEquals(testObserver.observedValues.size, 2, "Two view states should be received")
        assert(testObserver.observedValues[0] is Loading)
        assert(testObserver.observedValues[1] is Error)
        assert((testObserver.observedValues[1] as Error).exception == exception)
    }

    @Test
    fun `on fetchUsers - is successful - loading and success view states are received, verified expected result is filtered correctly`() {
        val response: FetchAlbumInfoResponse = mockFetchAlbumInfoResponse()
        val expected: List<Photo> = mockExpectedAlbumObj()
        val testObserver = viewModel.getViewState().testObserver()

        Mockito.`when`(useCase.execute()).thenReturn(Single.just(response))

        viewModel.fetchAlbumsInfo(1)

        assertEquals(testObserver.observedValues.size, 2, "Two view states should be received")
        assert(testObserver.observedValues[0] is Loading)
        assert(testObserver.observedValues[1] is Success<*>)
        assert((testObserver.observedValues[1] as Success<*>).data is List<*>)
        assertEquals(expected, (testObserver.observedValues[1] as Success<*>).data)
    }

    private fun mockExpectedAlbumObj(): List<Photo> = listOf(
        Photo(
            albumId = 1,
            id = 1,
            thumbnailUrl = "https://via.placeholder.com/150/92c952",
            title = "Random Image 1",
            url = "https://via.placeholder.com/150/92c952"
        ),
        Photo(
            albumId = 1,
            id = 2,
            thumbnailUrl = "https://via.placeholder.com/150/92c952",
            title = "Random Image 2",
            url = "https://via.placeholder.com/150/92c952"
        ),
        Photo(
            albumId = 1,
            id = 3,
            thumbnailUrl = "https://via.placeholder.com/150/92c952",
            title = "Random Image 3",
            url = "https://via.placeholder.com/150/92c952"
        )
    )

    private fun mockFetchAlbumInfoResponse(): FetchAlbumInfoResponse {
        val response = FetchAlbumInfoResponse()
        val list = arrayListOf(
            Photo(
                albumId = 1,
                id = 1,
                thumbnailUrl = "https://via.placeholder.com/150/92c952",
                title = "Random Image 1",
                url = "https://via.placeholder.com/150/92c952"
            ),
            Photo(
                albumId = 1,
                id = 2,
                thumbnailUrl = "https://via.placeholder.com/150/92c952",
                title = "Random Image 2",
                url = "https://via.placeholder.com/150/92c952"
            ),
            Photo(
                albumId = 1,
                id = 3,
                thumbnailUrl = "https://via.placeholder.com/150/92c952",
                title = "Random Image 3",
                url = "https://via.placeholder.com/150/92c952"
            ),
            Photo(
                albumId = 2,
                id = 4,
                thumbnailUrl = "https://via.placeholder.com/150/92c952",
                title = "Random Image 4",
                url = "https://via.placeholder.com/150/92c952"
            ),
            Photo(
                albumId = 3,
                id = 5,
                thumbnailUrl = "https://via.placeholder.com/150/92c952",
                title = "Random Image 5",
                url = "https://via.placeholder.com/150/92c952"
            ),
            Photo(
                albumId = 4,
                id = 6,
                thumbnailUrl = "https://via.placeholder.com/150/92c952",
                title = "Random Image 6",
                url = "https://via.placeholder.com/150/92c952"
            ),
            Photo(
                albumId = 5,
                id = 7,
                thumbnailUrl = "https://via.placeholder.com/150/92c952",
                title = "Random Image 6",
                url = "https://via.placeholder.com/150/92c952"
            ),
            Photo(
                albumId = 6,
                id = 8,
                thumbnailUrl = "https://via.placeholder.com/150/92c952",
                title = "Random Image 6",
                url = "https://via.placeholder.com/150/92c952"
            )
        )
        list.forEach {
            response.add(it)
        }
        return response
    }
}