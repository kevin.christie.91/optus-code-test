package com.optus.photoalbum.util

import com.optus.photoalbum.base.Schedulers
import io.reactivex.Scheduler


class TestSchedulers : Schedulers() {

    override fun ui(): Scheduler {
        return io.reactivex.schedulers.Schedulers.trampoline()
    }

    override fun io(): Scheduler {
        return io.reactivex.schedulers.Schedulers.trampoline()
    }
}