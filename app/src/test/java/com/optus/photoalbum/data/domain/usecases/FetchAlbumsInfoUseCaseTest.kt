package com.optus.photoalbum.data.domain.usecases

import com.nhaarman.mockito_kotlin.mock
import com.optus.photoalbum.base.Schedulers
import com.optus.photoalbum.data.api.models.response.fetchalbuminfo.FetchAlbumInfoResponse
import com.optus.photoalbum.data.domain.repositories.AlbumRepository
import com.optus.photoalbum.util.TestSchedulers
import io.reactivex.Single
import org.junit.Test
import org.mockito.Mockito

class FetchAlbumsInfoUseCaseTest {
    private val albumRepository: AlbumRepository = mock()
    private val schedulers: Schedulers = TestSchedulers()
    private val useCase =  FetchAlbumsInfoUseCase(albumRepository, schedulers)

    @Test
    fun `on execute - successful - api response is propagated`(){
        val fetchAlbumInfoResponse = FetchAlbumInfoResponse()

        Mockito.`when`(albumRepository.fetchAlbumsInfo()).thenReturn(Single.just(fetchAlbumInfoResponse))
        val testSubscriber = useCase.execute().test()

        testSubscriber.assertNoErrors()
        testSubscriber.assertComplete()
        testSubscriber.assertValue(fetchAlbumInfoResponse)
    }

    @Test
    fun `on execute - exception happened - exception is propagated`(){
        val exception = Throwable()
        Mockito.`when`(albumRepository.fetchAlbumsInfo()).thenReturn(Single.error(exception))

        val testSubscriber = useCase.execute().test()

        testSubscriber.assertError(exception)
        testSubscriber.assertNotComplete()
    }
}