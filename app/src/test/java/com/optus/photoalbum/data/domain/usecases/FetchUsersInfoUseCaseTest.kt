package com.optus.photoalbum.data.domain.usecases

import com.nhaarman.mockito_kotlin.mock
import com.optus.photoalbum.base.Schedulers
import com.optus.photoalbum.data.api.models.response.fetchuserinfo.FetchUsersInfoResponse
import com.optus.photoalbum.data.managers.UsersApiManager
import com.optus.photoalbum.util.TestSchedulers
import io.reactivex.Single
import org.junit.Test
import org.mockito.Mockito

class FetchUsersInfoUseCaseTest {
    private val userRepository: UsersApiManager = mock()
    private val schedulers: Schedulers = TestSchedulers()
    private val useCase =  FetchUsersInfoUseCase(userRepository, schedulers)

    @Test
    fun `on execute - successful - api response is propagated`(){
        val fetchUsersInfoResponse = FetchUsersInfoResponse()

        Mockito.`when`(userRepository.fetchUserInfo()).thenReturn(Single.just(fetchUsersInfoResponse))
        val testSubscriber = useCase.execute().test()

        testSubscriber.assertNoErrors()
        testSubscriber.assertComplete()
        testSubscriber.assertValue(fetchUsersInfoResponse)
    }

    @Test
    fun `on execute - exception happened - exception is propagated`(){
        val exception = Throwable()
        Mockito.`when`(userRepository.fetchUserInfo()).thenReturn(Single.error(exception))

        val testSubscriber = useCase.execute().test()

        testSubscriber.assertError(exception)
        testSubscriber.assertNotComplete()
    }
}