# Optus Photo Album

Optus photo album is an app built to showcase my skills for a code test by Optus. The requirements can be viewed [here.](https://drive.google.com/file/d/1wjOSnEgv_XP5OWvcF_pHaiijgxfJA-Cx/view?usp=sharing)

## Architecture

The app uses a Clean, MVVM architecture which utilizes LiveData, Data binding, Dagger (Dependency Injection), Retrofit+Rx for networking.
Dagger is used to resolve all dependencies accross the Presentation, Domain and Network layers. 
Data Binding is used in the activities and adapters for view binding. 
The Domain layer is split accross VMs and UseCases. Repositories provide a contract for Data managers to access the network layer and these contracts
link the domain layer to the data source (remote). VMs execute the UseCases to fetch and manipulate data models which are then provided to the Presentation
layer (Views) through LiveData observers.


## Project Structure:
The Project structure is as described below:
 - "base" Package: Contains all the necessary base and factory classes
 - "data" Package: Contains
    - "api" Package: Contains the following
        - "models" Package: Contains request/response data models together 
        - "services" Package: Contains the API services
        - "utils" Package: Contains some Network Util classes
    - "domain" Package consists of 
        - "exceptions" Package: Contains all the custom exception classes for handling varios application errors
        - "repositories" Package: Contains all the contracts for accessing API services
        - "usecases" Package: Contains UseCases which are used to access the data layer
    - "managers" Package: Contains data managers that are used to access the data source and provide the required data models
 - "di" Package: Contains all the dependency provider modules and Application Component
 - "presentation" Package: Contains the presentation layer packaged by screen/feature name adn error handling as follows:
    - "userinfo", "albuminfo", "photoinfo" Packages contain the Activities, ViewModels and ActivityModules of the presentation layer
    - "errorhandling" Package: Contains some useful error handling components (Dialog and Snackbar error components) that can be used hand in hand with 
                              for various scenarios
    - "extras" Package: Contains a few useful extension functions for creating snackbars and dialogs         

All the dependencies for various components are resolved in their respective modules that can be found in the "di" package.


To demonstrate my unit testing skills, I have writte 8 unit tests that covers positive and negative cases for VMs and UseCases and one simple UI test that 
verifies the E2E flow (under androidTest folder).

## Libraries Used:
 - Androidx and Androidx lifecycle extensions with Databinding enabled
 - Dagger
 - Retrofit
 - RxKotlin/RxAndroid/RxJava for use with Retrofit for network calls
 - Glide for Image loading

Testing frameworks used:
 - Espresso for UI tests (1 UI test to execute and E2E flow)
 - Mockito and Junit for unit tests
 - nhaarman_mockito - an external library that is less verbose than mockito for creating mocks/spies

 